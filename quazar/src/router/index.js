import Vue from 'vue'
import VueRouter from 'vue-router'
import Store from './../store/index'
import {sync} from 'vuex-router-sync'
import routes from './routes'



Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })


  // Sync Vuex and vue-router;
  sync(Store, Vue.router)

  Vue.use(VueAxios, axios)
  Vue.axios.defaults.baseURL = '/backend/api'

  

  Vue.use(auth, {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    rolesVar: 'role'
  })


  return Router
}
